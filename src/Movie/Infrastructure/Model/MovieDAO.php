<?php

namespace Alvaro\Videoclub\Movie\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;

class MovieDAO extends Model
{    

    protected $table = 'movies';    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image',
        'title',
        'synopsis',
        'genre',
        'status',
        'rental_date', 
        'return_date', 
        'user_created', 
        'user_updated', 
        'user_deleted', 
    ];    

}