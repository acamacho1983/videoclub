<?php

namespace Alvaro\Videoclub\Movie\Infrastructure\Persistence;

use Alvaro\Videoclub\Movie\Infrastructure\Model\MovieDAO;
use Alvaro\Videoclub\Movie\Domain\MovieEntity;
use Alvaro\Videoclub\Movie\Domain\MovieRepositoryInterface;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieId;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieImage;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieTitle;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieSynopsis;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieGenre;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieStatus;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieRentalDate;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieReturnDate;
use Alvaro\Videoclub\Movie\Domain\Exception\EntityNotFoundException;
Use Carbon\Carbon;

final class EloquentMovieRepository implements MovieRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(MovieEntity $movie)
    {         
        $movieInsert = MovieDAO::create([
            'image' => $movie->image(),
            'title' => $movie->title(), 
            'synopsis' => $movie->synopsis(), 
            'genre' => $movie->genre(),
            'status' => $movie->status(),
            'rentalDate' => $movie->rentalDate(), 
            'returnDate' => $movie->returnDate(), 
            'user_created' => auth()->user()->id, 
            'user_updated' => auth()->user()->id 
        ]); 
 
        return $this->transformMovieDAOToMovieEntity($movieInsert); 
    } 
    
    public function update(int $id, string $image, string $title, string $synopsis, string $genre, string $status, ?string $rentalDate, ?string $returnDate): void
    { 
        MovieDAO::where(['id' => $id])
        ->update([            
            'image' => $image,
            'title' => $title,
            'synopsis' => $synopsis,
            'genre' => $genre,
            'status' => $status,
            'rental_date' => $rentalDate,
            'return_date' => $returnDate
        ]); 
    }

    public function delete(int $id): void
    {
        MovieDAO::where(['id' => $id])
        ->update([            
            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s'), 
            'user_deleted' => auth()->user()->id 
        ]); 
    }

    public function findMovieById(int $id): MovieEntity
    { 
        $dao = MovieDAO::where([
            'id' => $id,
            'deleted_at' => null
        ])            
        ->first(); 

        if (is_null($dao)) { 
            return new MovieEntity(
                new MovieId(0),
                new MovieImage(null),
                new MovieTitle(''),
                new MovieSynopsis(''), 
                new MovieGenre(''),
                new MovieStatus(''),
                new MovieRentalDate(''),
                new MovieReturnDate('')
            );
        }
        
        return $this->transformMovieDAOToMovieEntity($dao);
    }

    public function findAllMovies($request)
    {     
        $query = MovieDAO::whereNull('deleted_at'); 
        
        if ($request->title()) {
            $query->where('title', 'like', '%'.$request->title().'%');
        }

        if ($request->genre()) {
            $query->where('genre', '=', $request->genre());
        }

        if (auth()->user()->getMoviesOnlyAvailable()) {
            $query->where('status', '=', 'available'); 
        } elseif ($request->status()) { 
            $query->where('status', '=', $request->status()); 
        } 

        if ($request->createdAtFrom()) {
            $query->where('created_at', '>=', Carbon::createFromFormat('Y-m-d', $request->createdAtFrom())->startOfDay());
        }

        if ($request->createdAtTo()) {
            $query->where('created_at', '<=', Carbon::createFromFormat('Y-m-d', $request->createdAtTo())->endOfDay());
        } 

        $dao = $query->get();
        
        $collection = $dao->map(function (MovieDAO $movieDAO) { 
            return $this->transformMovieDAOToMovieEntity($movieDAO);
        });         
 
        return $collection;
    }

    public function rent(int $id): void
    { 
        MovieDAO::where(['id' => $id])
        ->update([ 
            'status' => 'rented',
            'rental_date' => Carbon::now()->format('Y-m-d'),
            'return_date' => Carbon::now()->addDay(7)->format('Y-m-d')
        ]); 
    }

    public function return(int $id): void
    { 
        MovieDAO::where(['id' => $id])
        ->update([ 
            'status' => 'available',
            'rental_date' => null,
            'return_date' => null
        ]); 
    }

    private function transformMovieDAOToMovieEntity(MovieDAO $movieDAO): MovieEntity
    { 
        return new MovieEntity(
            new MovieId($movieDAO->id),
            new MovieImage($movieDAO->image),
            new MovieTitle($movieDAO->title),
            new MovieSynopsis($movieDAO->synopsis), 
            new MovieGenre($movieDAO->genre),
            new MovieStatus($movieDAO->status),
            new MovieRentalDate($movieDAO->rental_date),
            new MovieReturnDate($movieDAO->return_date)
        );
    }
}