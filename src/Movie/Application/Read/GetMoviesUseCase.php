<?php

namespace Alvaro\Videoclub\Movie\Application\Read;

use Alvaro\Videoclub\Movie\Domain\MovieEntity;
use Alvaro\Videoclub\Movie\Domain\MovieRepositoryInterface;

final class GetMoviesUseCase
{
    /**
     * @var MovieRepositoryInterface $movieRepository
     */
    private $movieRepository;

    public function __construct(MovieRepositoryInterface $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }

    public function execute($request)
    {                
        $response = $this->movieRepository->findAllMovies($request);

        return new GetMovieResponse($response->toArray());
    }
}