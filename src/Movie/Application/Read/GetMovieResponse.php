<?php

namespace Alvaro\Videoclub\Movie\Application\Read;

use Alvaro\Videoclub\Movie\Domain\MovieEntity;

final class GetMovieResponse
{
    private $movie;

    public function __construct(array $movie)
    {
        $this->movie = $movie;
    }

    public function getMovieDetail(): array
    {
        return array_map(function (MovieEntity $movie) {
            return [
                'id' => $movie->id(),
                'image' => $movie->image(),
                'title' => $movie->title(),
                'synopsis' => $movie->synopsis(),
                'genre' => $movie->genre(),
                'status' => $movie->status(),
                'rentalDate' => $movie->rentalDate(),
                'returnDate' => $movie->returnDate(),
            ];
        }, $this->movie);
    }    
}