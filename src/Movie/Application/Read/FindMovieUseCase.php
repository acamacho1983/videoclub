<?php

namespace Alvaro\Videoclub\Movie\Application\Read;

use Alvaro\Videoclub\Movie\Domain\MovieEntity;
use Alvaro\Videoclub\Movie\Domain\MovieRepositoryInterface;

final class FindMovieUseCase
{
    /**
     * @var MovieRepositoryInterface $movieRepository
     */
    private $movieRepository;

    public function __construct(MovieRepositoryInterface $movieRepository)
    {        
        $this->movieRepository = $movieRepository;
    }

    public function execute($id)
    {
        $response = $this->movieRepository->findMovieById($id);

        $responseArray = [
            'id' => $response->id(),
            'image' => $response->image(),
            'title' => $response->title(),
            'synopsis' => $response->synopsis(),
            'genre' => $response->genre(),
            'status' => $response->status(),
            'rentalDate' => $response->rentalDate(),
            'returnDate' => $response->returnDate()
        ];
        
        return $responseArray;
        
    }
}