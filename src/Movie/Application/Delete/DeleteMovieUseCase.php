<?php

namespace Alvaro\Videoclub\Movie\Application\Delete;

use Alvaro\Videoclub\Movie\Domain\MovieEntity;
use Alvaro\Videoclub\Movie\Domain\MovieRepositoryInterface;

final class DeleteMovieUseCase
{
    /**
     * @var MovieRepositoryInterface $movieRepository
     */
    private $movieRepository;

    public function __construct(MovieRepositoryInterface $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }

    public function execute($id)
    {
        $responseMovie = $this->movieRepository->findMovieById(
            $id
        ); 
        
        if ($responseMovie && auth()->user()->isAdmin()) {
            $this->movieRepository->delete(
                $id
            );

            return $responseMovie;
        }
    }
}