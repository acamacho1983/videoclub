<?php

namespace Alvaro\Videoclub\Movie\Application\Update;

use Alvaro\Videoclub\Movie\Domain\MovieEntity;
use Alvaro\Videoclub\Movie\Domain\MovieRepositoryInterface; 

final class ReturnMovieUseCase
{
    /**
     * @var MovieRepositoryInterface $movieRepository
     */
    private $movieRepository;

    public function __construct(MovieRepositoryInterface $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }        
    
    public function execute($id)
    { 
        $responseMovie = $this->movieRepository->findMovieById(
            $id
        ); 
                                
        if ($responseMovie && $responseMovie->status() == 'rented' && auth()->user()->isUser()) { 
            $this->movieRepository->return(
                $responseMovie->id()
            );
            
            return $responseMovie;
        }
    }
}