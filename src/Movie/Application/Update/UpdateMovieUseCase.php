<?php

namespace Alvaro\Videoclub\Movie\Application\Update;

use Alvaro\Videoclub\Movie\Domain\MovieEntity;
use Alvaro\Videoclub\Movie\Domain\MovieRepositoryInterface; 

final class UpdateMovieUseCase
{
    /**
     * @var MovieRepositoryInterface $movieRepository
     */
    private $movieRepository;

    public function __construct(MovieRepositoryInterface $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }        
    
    public function execute($request)
    { 
        $responseMovie = $this->movieRepository->findMovieById(
            $request->id()
        ); 
                                
        if ($responseMovie && auth()->user()->isAdmin()) { 
            $this->movieRepository->update(
                $responseMovie->id(),
                $request->image(),
                $request->title(),
                $request->synopsis(),
                $request->genre(),
                $request->status(),
                $request->rentalDate(),
                $request->returnDate()
            );
            
            return $responseMovie;
        }
    }
}