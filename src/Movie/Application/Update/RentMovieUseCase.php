<?php

namespace Alvaro\Videoclub\Movie\Application\Update;

use Alvaro\Videoclub\Movie\Domain\MovieEntity;
use Alvaro\Videoclub\Movie\Domain\MovieRepositoryInterface; 

final class RentMovieUseCase
{
    /**
     * @var MovieRepositoryInterface $movieRepository
     */
    private $movieRepository;

    public function __construct(MovieRepositoryInterface $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }        
    
    public function execute($id)
    { 
        $responseMovie = $this->movieRepository->findMovieById(
            $id
        ); 
                                
        if ($responseMovie && $responseMovie->status() == 'available' && auth()->user()->isUser()) { 
            $this->movieRepository->rent(
                $responseMovie->id()
            );
            
            return $responseMovie;
        }
    }
}