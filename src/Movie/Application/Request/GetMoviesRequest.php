<?php

namespace Alvaro\Videoclub\Movie\Application\Request;

use Illuminate\Support\Facades\Hash;

class GetMoviesRequest
{           
    private $title;    
    private $genre; 
    private $status; 
    private $createdAtFrom; 
    private $createdAtTo; 
        
    public function __construct(    
        $title,    
        $genre, 
        $status, 
        $createdAtFrom, 
        $createdAtTo 
    ) {        
        $this->title = $title;
        $this->genre = $genre; 
        $this->status = $status; 
        $this->createdAtFrom = $createdAtFrom;
        $this->createdAtTo = $createdAtTo;
    }    
        
    public function title(): ?string
    {
        return $this->title;
    }
    
    public function genre(): ?string
    {
        return $this->genre;
    }

    public function status(): ?string
    {
        return $this->status;
    }

    public function createdAtFrom(): ?string
    {
        return $this->createdAtFrom;
    } 

    public function createdAtTo(): ?string
    {
        return $this->createdAtTo;
    } 
}
