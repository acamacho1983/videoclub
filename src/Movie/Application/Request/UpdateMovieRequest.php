<?php

namespace Alvaro\Videoclub\Movie\Application\Request;

use Illuminate\Support\Facades\Hash;

class UpdateMovieRequest
{   
    private $id;    
    private $image;
    private $title;
    private $synopsis;
    private $genre; 
    private $status; 
    private $rentalDate; 
    private $returnDate; 
        
    public function __construct(    
        $id, 
        $image, 
        $title, 
        $synopsis, 
        $genre, 
        $status, 
        $rentalDate,
        $returnDate  
    ) {        
        $this->id = $id; 
        $this->image = $image; 
        $this->title = $title; 
        $this->synopsis = $synopsis; 
        $this->genre = $genre; 
        $this->status = $status; 
        $this->rentalDate = $rentalDate; 
        $this->returnDate = $returnDate; 
    }    

    public function id(): int
    {
        return $this->id;
    }
    
    public function image(): ?string
    {
        return (string)$this->image;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function synopsis(): string
    {
        return $this->synopsis;
    }

    public function genre(): string
    {
        return $this->genre;
    }

    public function status(): string
    {
        return $this->status;
    }

    public function rentalDate(): ?string
    {
        return $this->rentalDate;
    } 

    public function returnDate(): ?string
    {
        return $this->returnDate;
    } 
}
