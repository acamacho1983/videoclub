<?php

namespace Alvaro\Videoclub\Movie\Application\Create;

use Alvaro\Videoclub\Movie\Domain\MovieEntity;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieId;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieImage;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieTitle;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieSynopsis;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieGenre;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieStatus;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieRentalDate;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieReturnDate;
use Alvaro\Videoclub\Movie\Domain\MovieRepositoryInterface; 

final class CreateMovieUseCase
{
    /**
     * @var MovieRepositoryInterface $movieRepository
     */
    private $movieRepository;

    public function __construct(MovieRepositoryInterface $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }

    public function execute($request)
    {   
        if (auth()->user()->isAdmin()) {            
            $movie = new MovieEntity( 
                new MovieId(0),            
                new MovieImage($request->image()),
                new MovieTitle($request->title()),
                new MovieSynopsis($request->synopsis()),
                new MovieGenre($request->genre()),
                new MovieStatus($request->status()),
                new MovieRentalDate($request->rentalDate()),
                new MovieReturnDate($request->returnDate())
            ); 
    
            $response = $this->movieRepository->create($movie); 

            return $response; 
        } 
    }    
}