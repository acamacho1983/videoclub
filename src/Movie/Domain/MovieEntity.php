<?php

namespace Alvaro\Videoclub\Movie\Domain;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieId;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieImage;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieTitle;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieSynopsis;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieGenre;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieStatus;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieRentalDate;
use Alvaro\Videoclub\Movie\Domain\ValueObject\MovieReturnDate;
use Alvaro\Videoclub\Shared\Infrastructure\Interfaces\Arrayable;

final class MovieEntity
{   
    private $id;
    private $image;
    private $title;
    private $synopsis;
    private $genre; 
    private $status; 
    private $rentalDate; 
    private $returnDate; 
        
    public function __construct(    
        MovieId $id,
        MovieImage $image, 
        MovieTitle $title, 
        MovieSynopsis $synopsis, 
        MovieGenre $genre, 
        MovieStatus $status, 
        MovieRentalDate $rentalDate,
        MovieReturnDate $returnDate  
    ) { 
        $this->id = $id;        
        $this->image = $image; 
        $this->title = $title; 
        $this->synopsis = $synopsis; 
        $this->genre = $genre; 
        $this->status = $status; 
        $this->rentalDate = $rentalDate; 
        $this->returnDate = $returnDate; 
    }

    public function id(): int
    {
        return $this->id->get();
    }

    public function image(): ?string
    {
        return (string)$this->image;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function synopsis(): string
    {
        return $this->synopsis;
    }

    public function genre(): string
    {
        return $this->genre;
    }

    public function status(): string
    {
        return $this->status;
    }

    public function rentalDate(): ?string
    {
        return $this->rentalDate;
    } 

    public function returnDate(): ?string
    {
        return $this->returnDate;
    } 
}
