<?php

namespace Alvaro\Videoclub\Movie\Domain;

interface MovieRepositoryInterface
{
    /**
     * Persist a new movie instance
     *
     * @param Movie $movie
     *
     * @return void
     */
    public function create(MovieEntity $movie); 

    public function update(
        int $id,
        string $image,
        string $title,
        string $synopsis,
        string $genre,
        string $status,
        ?string $rentalDate,
        ?string $returnDate
    ): void;

    public function delete(
        int $id
    ): void;

    public function findMovieById(
        int $id
    );

    public function findAllMovies($request);

    public function rent(
        int $id 
    ): void;

    public function return(
        int $id 
    ): void;
    
}