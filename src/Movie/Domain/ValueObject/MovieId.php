<?php

namespace Alvaro\Videoclub\Movie\Domain\ValueObject;

class MovieId 
{
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function get(): int
    {
        return $this->id;
    }
        
}
