<?php

namespace Alvaro\Videoclub\Movie\Domain\ValueObject;

final class MovieGenre
{
    private $genre;

    public function __construct(string $genre)
    {
        $this->genre = $genre;
    }

    public function get(): string
    {
        return $this->genre;
    }

    public function __toString()
    {
        return $this->genre;
    }
}