<?php

namespace Alvaro\Videoclub\Movie\Domain\ValueObject;

final class MovieRentalDate
{
    private $rentalDate;

    public function __construct(?string $rentalDate)
    {
        $this->rentalDate = $rentalDate;
    }

    public function get(): ?string
    {
        return $this->rentalDate;
    }

    public function __toString()
    {
        return (string)$this->rentalDate;
    }
}