<?php

namespace Alvaro\Videoclub\Movie\Domain\ValueObject;

final class MovieTitle
{
    private $title;

    public function __construct(string $title)
    {
        $this->title = $title;
    }

    public function get(): string
    {
        return $this->title;
    }

    public function __toString()
    {
        return $this->title;
    }
}