<?php

namespace Alvaro\Videoclub\Movie\Domain\ValueObject;

final class MovieImage
{
    private $image;

    public function __construct(?string $image)
    {
        $this->image = $image;
    }

    public function get(): string
    {
        return $this->image;
    }

    public function __toString()
    {
        return (string)$this->image;
    }
}