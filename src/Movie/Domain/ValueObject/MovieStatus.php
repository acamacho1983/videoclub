<?php

namespace Alvaro\Videoclub\Movie\Domain\ValueObject;

final class MovieStatus
{
    private $status;

    public function __construct(string $status)
    {
        $this->status = $status;
    }

    public function get(): string
    {
        return $this->status;
    }

    public function __toString()
    {
        return $this->status;
    }
}