<?php

namespace Alvaro\Videoclub\Movie\Domain\ValueObject;

final class MovieSynopsis
{
    private $synopsis;

    public function __construct(string $synopsis)
    {
        $this->synopsis = $synopsis;
    }

    public function get(): string
    {
        return $this->synopsis;
    }

    public function __toString()
    {
        return $this->synopsis;
    }
}