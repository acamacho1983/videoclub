<?php

namespace Alvaro\Videoclub\Movie\Domain\ValueObject;

final class MovieReturnDate
{
    private $returnDate;

    public function __construct(?string $returnDate)
    {
        $this->returnDate = $returnDate;
    }

    public function get(): ?string
    {
        return $this->returnDate;
    }

    public function __toString()
    {
        return (string)$this->returnDate;
    }
}