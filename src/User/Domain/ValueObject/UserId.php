<?php

namespace Alvaro\Videoclub\User\Domain\ValueObject;

class UserId 
{
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function get(): int
    {
        return $this->id;
    }
        
}
