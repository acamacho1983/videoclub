<?php

namespace Alvaro\Videoclub\User\Domain\ValueObject;

final class UserEmail
{
    private $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public function get(): string
    {
        return $this->email;
    }

    public function __toString()
    {
        return $this->email;
    }
}