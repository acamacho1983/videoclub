<?php

namespace Alvaro\Videoclub\User\Domain\ValueObject;

final class UserFirstSurname
{
    private $firstSurname;

    public function __construct(string $firstSurname)
    {
        $this->firstSurname = $firstSurname;
    }

    public function get(): string
    {
        return $this->firstSurname;
    }

    public function __toString()
    {
        return $this->firstSurname;
    }
}