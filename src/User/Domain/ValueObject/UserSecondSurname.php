<?php

namespace Alvaro\Videoclub\User\Domain\ValueObject;

final class UserSecondSurname
{
    private $secondSurname;

    public function __construct(?string $secondSurname)
    {
        $this->secondSurname = $secondSurname;
    }

    public function get(): ?string
    {
        return $this->secondSurname;
    }

    public function __toString()
    {
        return (string)$this->secondSurname;
    }
}