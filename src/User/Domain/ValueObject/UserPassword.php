<?php

namespace Alvaro\Videoclub\User\Domain\ValueObject;

final class UserPassword
{
    private $password;

    public function __construct(string $password)
    {
        $this->password = $password;
    }

    public function get(): string
    {
        return $this->password;
    }

    public function __toString()
    {
        return $this->password;
    }
}