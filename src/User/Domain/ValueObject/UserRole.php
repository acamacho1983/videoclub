<?php

namespace Alvaro\Videoclub\User\Domain\ValueObject;

final class UserRole
{
    private $role;

    public function __construct(string $role)
    {
        $this->role = $role;
    }

    public function get(): string
    {
        return $this->role;
    }

    public function __toString()
    {
        return $this->role;
    }
}