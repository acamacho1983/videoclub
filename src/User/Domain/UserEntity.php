<?php

namespace Alvaro\Videoclub\User\Domain;
use Alvaro\Videoclub\User\Domain\ValueObject\UserId;
use Alvaro\Videoclub\User\Domain\ValueObject\UserName;
use Alvaro\Videoclub\User\Domain\ValueObject\UserFirstSurname;
use Alvaro\Videoclub\User\Domain\ValueObject\UserSecondSurname;
use Alvaro\Videoclub\User\Domain\ValueObject\UserRole;
use Alvaro\Videoclub\User\Domain\ValueObject\UserEmail;
use Alvaro\Videoclub\User\Domain\ValueObject\UserPassword;
use Alvaro\Videoclub\Shared\Infrastructure\Interfaces\Arrayable;

final class UserEntity
{   
    private $id; 
    private $name;
    private $firstSurname;
    private $secondSurname;
    private $role; 
    private $email; 
    private $password;     
        
    public function __construct(
        UserId $id,        
        UserName $name,
        UserFirstSurname $firstSurname,
        UserSecondSurname $secondSurname,
        UserRole $role, 
        UserEmail $email, 
        UserPassword $password 
    ) {
        $this->id = $id; 
        $this->name = $name; 
        $this->firstSurname = $firstSurname; 
        $this->secondSurname = $secondSurname; 
        $this->role = $role; 
        $this->email = $email; 
        $this->password = $password; 
    }

    public function id(): int
    {
        return $this->id->get();
    }
    
    public function name(): string
    {
        return $this->name;
    }

    public function firstSurname(): string
    {
        return $this->firstSurname;
    }

    public function secondSurname(): ?string
    {
        return $this->secondSurname;
    }

    public function role(): string
    {
        return $this->role;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function password(): ?string
    {
        return $this->password;
    } 
}
