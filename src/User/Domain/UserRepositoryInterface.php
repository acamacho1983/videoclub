<?php

namespace Alvaro\Videoclub\User\Domain;

interface UserRepositoryInterface
{
    /**
     * Persist a new user instance
     *
     * @param User $user
     *
     * @return void
     */
    public function create(UserEntity $user); 
    
}