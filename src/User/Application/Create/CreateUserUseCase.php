<?php

namespace Alvaro\Videoclub\User\Application\Create;

use Alvaro\Videoclub\User\Domain\UserEntity;
use Alvaro\Videoclub\User\Domain\ValueObject\UserId;
use Alvaro\Videoclub\User\Domain\ValueObject\UserName;
use Alvaro\Videoclub\User\Domain\ValueObject\UserFirstSurname;
use Alvaro\Videoclub\User\Domain\ValueObject\UserSecondSurname;
use Alvaro\Videoclub\User\Domain\ValueObject\UserRole;
use Alvaro\Videoclub\User\Domain\ValueObject\UserEmail;
use Alvaro\Videoclub\User\Domain\ValueObject\UserPassword;
use Alvaro\Videoclub\User\Domain\UserRepositoryInterface;

final class CreateUserUseCase
{
    /**
     * @var UserRepositoryInterface $userRepository
     */
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function execute($request)
    {         
        $user = new UserEntity( 
            new UserId(0),            
            new UserName($request->name()),
            new UserFirstSurname($request->firstSurname()),
            new UserSecondSurname($request->secondSurname()),
            new UserRole($request->role()),
            new UserEmail($request->email()),
            new UserPassword($request->password())
        ); 

        return $this->userRepository->create($user);        
    }    
}