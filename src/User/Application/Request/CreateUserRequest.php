<?php

namespace Alvaro\Videoclub\User\Application\Request;

use Illuminate\Support\Facades\Hash;

class CreateUserRequest
{       
    private $name;
    private $firstSurname;
    private $secondSurname;
    private $role; 
    private $email; 
    private $password;        
        
    public function __construct(    
        $name, 
        $firstSurname, 
        $secondSurname, 
        $role, 
        $email, 
        $password 
    ) {        
        $this->name = $name; 
        $this->firstSurname = $firstSurname; 
        $this->secondSurname = $secondSurname; 
        $this->role = $role; 
        $this->email = $email; 
        $this->password = $password; 
    }    
    
    public function name(): string
    {
        return $this->name;
    }

    public function firstSurname(): string
    {
        return $this->firstSurname;
    }

    public function secondSurname(): ?string
    {
        return $this->secondSurname;
    }

    public function role(): string
    {
        return $this->role;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function password(): ?string
    {
        return bcrypt($this->password);
    } 
}
