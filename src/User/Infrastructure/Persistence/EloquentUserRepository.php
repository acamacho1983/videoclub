<?php

namespace Alvaro\Videoclub\User\Infrastructure\Persistence;

use Alvaro\Videoclub\User\Infrastructure\Model\UserDAO;
use Alvaro\Videoclub\User\Domain\UserEntity;
use Alvaro\Videoclub\User\Domain\UserRepositoryInterface;
use Alvaro\Videoclub\User\Domain\ValueObject\UserId;
use Alvaro\Videoclub\User\Domain\ValueObject\UserName;
use Alvaro\Videoclub\User\Domain\ValueObject\UserFirstSurname;
use Alvaro\Videoclub\User\Domain\ValueObject\UserSecondSurname;
use Alvaro\Videoclub\User\Domain\ValueObject\UserRole;
use Alvaro\Videoclub\User\Domain\ValueObject\UserEmail;
use Alvaro\Videoclub\User\Domain\ValueObject\UserPassword;
Use Carbon\Carbon;

final class EloquentUserRepository implements UserRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(UserEntity $user)
    {
        $userInsert = UserDAO::create([                          
            'name' => $user->name(),
            'first_surname' => $user->firstSurname(), 
            'second_surname' => $user->secondSurname(), 
            'role' => $user->role(),
            'email' => $user->email(),
            'password' => $user->password()
        ]);

        return $this->transformUserDAOToUserEntity($userInsert); 
    }   
    
    private function transformUserDAOToUserEntity(UserDAO $userDAO): UserEntity
    { 
        return new UserEntity(
            new UserId($userDAO->id),
            new UserName($userDAO->name),
            new UserFirstSurname($userDAO->first_surname),
            new UserSecondSurname($userDAO->second_surname), 
            new UserRole($userDAO->role),
            new UserEmail($userDAO->email),
            new UserPassword($userDAO->password)
        );
    }
    
}