<?php

namespace Alvaro\Videoclub\User\Infrastructure\Model;

use Illuminate\Database\Eloquent\Model;

class UserDAO extends Model
{    
    
    protected $table = 'users';    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'first_surname',
        'second_surname',
        'role',
        'email',
        'password', 
    ];    

}