<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Alvaro\Videoclub\Movie\Infrastructure\Model\MovieDAO;

class MovieManagementTest extends TestCase
{    
    // use RefreshDatabase;    

    /** @test */
    public function a_movie_can_be_created()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/movie', [
            'image' => 'Image movie',
            'title' => 'Title movie',
            'synopsis' => 'Synopsis movie',
            'genre' => 'action',
            'status' => 'available',
            'rental_date' => '',
            'return_date' => ''
        ]);

        $response->assertOk(); 
        
        $movie = MovieDAO::find($response->baseResponse->original['data']['id']); 

        $this->assertEquals($movie->image, 'Image movie'); 
        $this->assertEquals($movie->title, 'Title movie'); 
        $this->assertEquals($movie->synopsis, 'Synopsis movie'); 
        $this->assertEquals($movie->genre, 'action'); 
        $this->assertEquals($movie->status, 'available'); 
        $this->assertEquals($movie->rental_date, ''); 
        $this->assertEquals($movie->return_date, ''); 
    }

    /** @test */
    public function list_of_movies_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $response = $this->get('/api/movies');

        $response->assertOk(); 
                
        $response->assertSee('Image movie');
        $response->assertSee('Title movie');
        $response->assertSee('Synopsis movie');
        $response->assertSee('action');
        $response->assertSee('available');        
    }

    /** @test */
    public function a_movie_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        $movie = MovieDAO::orderBy('id', 'desc')->first();

        $response = $this->get('/api/movie/'.$movie->id); 

        $this->assertEquals($response->baseResponse->original->image, 'Image movie');
        $this->assertEquals($response->baseResponse->original->title, 'Title movie');
        $this->assertEquals($response->baseResponse->original->synopsis, 'Synopsis movie');
        $this->assertEquals($response->baseResponse->original->genre, 'action');        
        $this->assertEquals($response->baseResponse->original->status, 'available');                      
    }

    /** @test */
    public function a_movie_can_be_updated()
    {
        $this->withoutExceptionHandling();

        $movie = MovieDAO::orderBy('id', 'desc')->first(); 

        $response = $this->put('/api/movie/'.$movie->id, [ 
            'image' => $movie->image.' -- update',
            'title' => $movie->title.' -- update', 
            'synopsis' => $movie->synopsis.' -- update', 
            'genre' => 'comedy', 
            'status' => 'rented', 
            'rental_date' => '2021-11-01', 
            'return_date' => '2021-12-01'             
        ]); 

        $response->assertOk(); 

        $movieUpdate = MovieDAO::find($movie->id);
                
        $this->assertEquals($movieUpdate->image, $movie->image.' -- update'); 
        $this->assertEquals($movieUpdate->title, $movie->title.' -- update'); 
        $this->assertEquals($movieUpdate->synopsis, $movie->synopsis.' -- update'); 
        $this->assertEquals($movieUpdate->genre, 'comedy'); 
        $this->assertEquals($movieUpdate->status, 'rented'); 
        $this->assertEquals($movieUpdate->rental_date, '2021-11-01'); 
        $this->assertEquals($movieUpdate->return_date, '2021-12-01');         
    }

    /** @test */
    public function a_movie_can_be_deleted()
    {
        $this->withoutExceptionHandling(); 

        $response = $this->post('/api/movie', [
            'image' => 'Image Movie delete',
            'title' => 'Title Movie delete',
            'synopsis' => 'Synopsis Movie delete',
            'genre' => 'action',
            'status' => 'available',
            'rental_date' => '',
            'return_date' => ''             
        ]);

        $response->assertOk(); 
        
        $response = $this->delete('/api/movie/'.$response->baseResponse->original['data']['id']); 

        $response->assertOk(); 
    }

    /** @test */
    public function movie_title_is_required()
    { 
        $response = $this->post('/api/movie', [
            'image' => 'Image movie',
            'title' => '',
            'synopsis' => 'Synopsis movie',
            'genre' => 'action',
            'status' => 'available'             
        ]);

        $response->assertSessionHasErrors(['title']); 
    }    
}
