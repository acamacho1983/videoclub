function registerUser()
{    
    $.ajax({
        url: `/api/register`,
        type: 'post',
        dataType: 'JSON',
        data: {
            name: $('#name').val(),
            first_surname: $('#first_surname').val(),
            second_surname: $('#second_surname').val(),
            email: $('#email').val(),
            password: $('#password').val(),
            password_confirmation: $('#password_confirmation').val(),
        }, 
        success: function (response) { 
            if (response.success) { 
                AlertMessage("", response.message).then(function() {
                    window.location.href = `/`;
                });
            } else {
                AlertMessage("", response.message);
            }
        }
    });
}