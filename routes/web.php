<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { 
    return view('auth.login');
})->name('login.show');

Route::get('/register', function () {
    return view('auth.register');
})->name('register.show');

Route::get('/login', [App\Http\Controllers\Web\User\Read\LoginController::class, '__invoke'])->name('login.user');

Route::group(['middleware' => 'api'], function() {
    Route::get('/movies', [App\Http\Controllers\Web\Movie\Read\MovieController::class, '__invoke'])->name('movie.index'); 
    Route::get('/home', [App\Http\Controllers\Web\Home\HomeController::class, '__invoke'])->name('home'); 
});

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
