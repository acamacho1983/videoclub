<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/login', [App\Http\Controllers\Api\User\Read\LoginUserController::class, '__invoke'])->name('login');
Route::post('/register', [App\Http\Controllers\Api\User\Create\RegisterUserController::class, '__invoke'])->name('register.create');

Route::group(['middleware' => 'api'], function() {
    Route::get('/movies', [App\Http\Controllers\Api\Movie\Read\GetMoviesController::class, '__invoke'])->name('movie.index'); 
    Route::get('/movies/{movie}', [App\Http\Controllers\Api\Movie\Read\FindMovieController::class, '__invoke'])->name('movie.find'); 
    Route::post('/movies', [App\Http\Controllers\Api\Movie\Create\CreateMovieController::class, '__invoke'])->name('movie.create'); 
    Route::put('/movies/{movie}', [App\Http\Controllers\Api\Movie\Update\UpdateMovieController::class, '__invoke'])->name('movie.update'); 
    Route::put('/movies/{movie}/rent', [App\Http\Controllers\Api\Movie\Update\RentMovieController::class, '__invoke'])->name('movie.rent'); 
    Route::put('/movies/{movie}/return', [App\Http\Controllers\Api\Movie\Update\ReturnMovieController::class, '__invoke'])->name('movie.return'); 
    Route::delete('/movies/{movie}', [App\Http\Controllers\Api\Movie\Delete\DeleteMovieController::class, '__invoke'])->name('movie.delete'); 
});
