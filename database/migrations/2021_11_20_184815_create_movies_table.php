<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->string('image', 20)->nullable(); 
            $table->string('title', 255); 
            $table->text('synopsis'); 
            $table->enum('genre', ['action', 'comedy', 'drama', 'suspense', 'terror', 'actual_facts', 'science_fiction', 'adventures']); 
            $table->enum('status', ['available', 'rented'])->default('available'); 
            $table->date('rental_date')->nullable(); 
            $table->date('return_date')->nullable(); 
            $table->timestamps(); 
            $table->softDeletes(); 
            $table->bigInteger('user_created')->unsigned()->index('IDX_movies_user_created'); 
            $table->bigInteger('user_updated')->unsigned()->index('IDX_movies_user_updated'); 
            $table->bigInteger('user_deleted')->unsigned()->nullable()->index('IDX_movies_user_deleted'); 

            $table->foreign('user_created', 'FK_products__user_created')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('user_updated', 'FK_products__user_updated')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('user_deleted', 'FK_products__user_deleted')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
