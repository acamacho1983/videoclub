<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
Use Carbon\Carbon;
use Alvaro\Videoclub\User\Application\Request\CreateUserRequest;
use Alvaro\Videoclub\User\Application\Create\CreateUserUseCase;

class InsertUsersSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * php artisan db:seed --class=InsertUsersSeeder
     * 
     * @return void
     */
    public function run(CreateUserUseCase $useCase)
    {
        DB::beginTransaction();
        try {

            $user = $useCase->execute(
                new CreateUserRequest(                
                    'Administrador', 
                    '.', 
                    null, 
                    'admin', 
                    'admin@nomail.es', 
                    '12345678aA'
                )
            );

            DB::commit();
        } catch (\Throwable $error) { 
            DB::rollback();            
        }        
    }
}
