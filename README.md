#### INSTALACIÓN

 - Clonar el repositorio.
 - Crear el archivo `.env` (ejemplo en `.env.example`).
 - Instalar las dependencias `composer install`. 
 - Ejecutar las migraciones `php artisan migrate`.
 - Ejecutar `php artisan key:generate`.
 - Ejecutar `php artisan jwt:secret`.
 - Ejecutar `php artisan db:seed --class=InsertUsersSeeder` para crear usuario administrador.
 - Validar instalación con los test `php artisan test`.
----

#### REQUISISTOS

 - PHP 7.3
 - Mysql
