@extends('layouts.app')


<div class="panel panel-default">                        
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Availables Films</h1>
            </div>                
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>                                
                                <th>Título</th>
                                <th>Estado</th>
                                <th>Género</th>                                
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($movies as $movie)
                        <tr>
                            <td>{{ $movie->title }}</td>
                            <td>{{ $movie->status }}</td>
                            <td>{{ $movie->genre }}</td>                            
                        </tr>
                        @endforeach                                                                                                       
                        </tbody>
                    </table>
                </div>                            
            </div>
        </div>
    </div>                
</div> 