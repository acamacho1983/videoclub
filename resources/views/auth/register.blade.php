@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ __('messages.login.register') }}</h3>
                </div>
                <div class="panel-body">
                    <form role="form">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="{{ __('messages.login.name') }}" name="name" id="name" type="text">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="{{ __('messages.login.first_surname') }}" name="first_surname" id="first_surname" type="text">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="{{ __('messages.login.second_surname') }}" name="second_surname" id="second_surname" type="text">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="{{ __('messages.login.email') }}" name="email" id="email" type="email" autofocus>
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="{{ __('messages.login.password') }}" name="password" id="password" type="password" value="">
                            </div>  

                            <div class="form-group">
                                <input class="form-control" placeholder="{{ __('messages.login.confirm_password') }}" name="password_confirmation" id="password_confirmation" type="password" value="">
                            </div>
                            <!-- Change this to a button or input when using this as a form -->
                            <a href="#" onclick="registerUser();" class="btn btn-lg btn-success btn-block">{{ __('messages.login.register') }}</a>                            
                            
                        </fieldset>
                    </form>
                </div>   
                <div class="panel-body">
                    <a class="text-success" href="{{ route('login.show') }}">{{ __('messages.login.sign_in') }}</a>
                </div>             
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('/js/user/user.js') }}"></script>
@endpush