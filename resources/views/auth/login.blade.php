@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ __('messages.login.please_sign_in') }}</h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="{{ route('login.user') }}" method="GET">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="{{ __('messages.login.email') }}" name="email" type="email" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="{{ __('messages.login.password') }}" name="password" type="password" value="">
                            </div>                            
                            <!-- Change this to a button or input when using this as a form -->
                            <input type="submit" value="{{ __('messages.login.sign_in') }}" class="btn btn-lg btn-success btn-block">
                            
                        </fieldset>
                    </form>
                </div>
                <div class="panel-body">
                    <a class="text-success" href="{{ route('register.show') }}">{{ __('messages.login.register') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
