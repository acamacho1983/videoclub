<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => [
        'please_sign_in' => 'Por favor inicie sesión',
        'sign-in' => 'Iniciar sesión',
        'register' => 'Registrarse',  
        'email' => 'Email',  
        'password' => 'Contraseña',  
    ]      

];
