<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'common' => [
        'save_data_success' => 'Data saved correctly',
        'delete_data_success' => 'Data deleted correctly'
    ],  
    'login' => [
        'invalidCredentials' => 'Invalid Credentials',
        'please_sign_in' => 'Please Sign In',
        'sign_in' => 'Sign In',
        'register' => 'Register',  
        'email' => 'Email',  
        'password' => 'Password',  
        'confirm_password' => 'Confirm Password',  
        'name' => 'Name',  
        'first_surname' => 'First Surname',  
        'second_surname' => 'Second Surname',  
    ],
    'register' => [
        'correctly_registered' => 'Registration successfully completed.<br><br> You can now access the platform.'
    ]

];
