<?php

namespace App\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:3', 'max:255'], 
            'first_surname' => ['required', 'min:3', 'max:255'], 
            'second_surname' => ['min:3', 'max:255'], 
            'email' => ['required', 'email', 'max:255', 'unique:users,email,{id},id,deleted_at,NULL'], 
            'password' => ['string', 'min:8', 'confirmed', 'regex:/[a-z]/', 'regex:/[A-Z]/', 'regex:/[0-9]/'], 
        ];        
    }
}
