<?php

namespace App\Requests\Movie;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditMovieRequest extends FormRequest
{
    /**
     * Determine if the movie is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => ['min:3', 'max:20'], 
            'title' => ['required', 'min:3', 'max:255'], 
            'synopsis' => ['required', 'min:3'], 
            'genre' => ['required', 'in:action,comedy,drama,suspense,terror,actual_facts,science_fiction,adventures'], 
            'status' => ['required', 'in:available,rented'], 
            'rental_date' => ['date'], 
            'return_date' => ['date'],             
        ];        
    }
}
