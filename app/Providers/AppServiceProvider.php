<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    private $bindingInterfaces = [
        \Alvaro\Videoclub\User\Domain\UserRepositoryInterface::class => \Alvaro\Videoclub\User\Infrastructure\Persistence\EloquentUserRepository::class, 
        \Alvaro\Videoclub\Movie\Domain\MovieRepositoryInterface::class => \Alvaro\Videoclub\Movie\Infrastructure\Persistence\EloquentMovieRepository::class, 
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->bindingInterfaces as $interface => $implementation) {
            $this->app->bind($interface, $implementation);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
