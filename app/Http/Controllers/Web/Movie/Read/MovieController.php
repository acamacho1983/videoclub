<?php

namespace App\Http\Controllers\Web\Movie\Read;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Alvaro\Videoclub\Movie\Infrastructure\Model\MovieDAO;

class MovieController extends Controller
{        
    /**
     * Home.
     *     
     * @return \Illuminate\Http\Response
     */    
    public function __invoke(Request $request) 
    { 
        $movies = MovieDAO::where('status', 'available')->get();

        return view('movies.index', [
            'movies' => $movies
        ]); 
    }
}
