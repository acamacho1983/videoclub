<?php

namespace App\Http\Controllers\Web\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{    
    /**
     * Home.
     *     
     * @return \Illuminate\Http\Response
     */    
    public function __invoke(Request $request) 
    { 
        return view('welcome'); 
    }
}
