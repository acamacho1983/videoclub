<?php

namespace App\Http\Controllers\Web\User\Read;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Http;

class LoginController extends Controller
{    
    /**
     * Login user.
     *     
     * @return \Illuminate\Http\Response
     */    
    public function __invoke(Request $request) 
    {         
        $request = \Request::create('/api/login', 'GET', [
                'email' => $request->email,
                'password' => $request->password
        ]);

        $response = \Route::dispatch($request)->original; 
                
        $url = "home";        
                                        
        if (auth()->user()->isUser()) {
            $url = "movies";
        } 

        return redirect($url);
    }
}
