<?php

namespace App\Http\Controllers\Api\Movie\Create;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Requests\Movie\StoreMovieRequest;
use Alvaro\Videoclub\Movie\Application\Request\CreateMovieRequest;
use Alvaro\Videoclub\Movie\Application\Create\CreateMovieUseCase;

class CreateMovieController extends Controller
{
    /**
     * Create new movie.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(StoreMovieRequest $request, CreateMovieUseCase $useCase)    
    { 
        $message = trans('messages.common.save_data_success'); 
        $success = true;
        $code = 200;
        $movieInsert = null;
        
        DB::beginTransaction();
        try {
            if (auth()->user()->isAdmin()) {
                $movieInsert = $useCase->execute(
                    new CreateMovieRequest(                
                        $request->image, 
                        $request->title,                     
                        $request->synopsis,                     
                        $request->genre, 
                        $request->status,
                        $request->rental_date,
                        $request->return_date
                    )
                ); 

                DB::commit();
            } else {
                $success = false;
                $message = "notAuthorized";
                $code = 403;
            }
        } catch (\Throwable $error) {
            $success = false;
            $message = $error->getMessage();
            $code = 500;
            DB::rollback();            
        } 

        return createResponse($success, $message, $code, ['id' => $movieInsert ? $movieInsert->id() : null]);
    }
}
