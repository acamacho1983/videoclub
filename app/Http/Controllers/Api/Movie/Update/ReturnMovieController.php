<?php

namespace App\Http\Controllers\Api\Movie\Update;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Alvaro\Videoclub\Movie\Infrastructure\Model\MovieDAO;
use Alvaro\Videoclub\Movie\Application\Update\ReturnMovieUseCase;

class ReturnMovieController extends Controller
{
    /**
     * Update new movie.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(MovieDAO $movie, ReturnMovieUseCase $useCase)    
    { 
        $message = trans('messages.common.save_data_success'); 
        $success = true;
        $code = 200;
        $rentMovie = null;
        
        DB::beginTransaction();
        try {

            if ($movie->status == 'rented' && auth()->user()->isUser()) { 
                $rentMovie = $useCase->execute($movie->id);

                if ($rentMovie->id() == 0) {
                    return createResponse(false, 'notFound', 404); 
                } 
    
                DB::commit(); 
            } else {
                $success = false;
                $message = "notAuthorized";
                $code = 403;
            }                        
        } catch (\Throwable $error) {
            $success = false;
            $message = $error->getMessage();
            $code = 500;
            DB::rollback();            
        }        

        return createResponse($success, $message, $code, ['id' => $rentMovie ? $rentMovie->id() : null]);
    }
}
