<?php

namespace App\Http\Controllers\Api\Movie\Update;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Requests\Movie\EditMovieRequest;
use Alvaro\Videoclub\Movie\Infrastructure\Model\MovieDAO;
use Alvaro\Videoclub\Movie\Application\Request\UpdateMovieRequest;
use Alvaro\Videoclub\Movie\Application\Update\UpdateMovieUseCase;

class UpdateMovieController extends Controller
{
    /**
     * Update new movie.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(MovieDAO $movie, EditMovieRequest $request, UpdateMovieUseCase $useCase)    
    { 
        $message = trans('messages.common.save_data_success'); 
        $success = true;
        $code = 200;
        $movieUpdate = null;
        
        DB::beginTransaction();
        try {

            if (auth()->user()->isAdmin()) { 
                $movieUpdate = $useCase->execute(
                    new UpdateMovieRequest( 
                        $movie->id, 
                        $request->image, 
                        $request->title,                     
                        $request->synopsis,                     
                        $request->genre, 
                        $request->status,
                        $request->rental_date,
                        $request->return_date
                    )
                );

                if ($movieUpdate->id() == 0) {
                    return createResponse(false, 'notFound', 404); 
                } 
    
                DB::commit(); 
            } else {
                $success = false;
                $message = "notAuthorized";
                $code = 403;
            }                        
        } catch (\Throwable $error) {
            $success = false;
            $message = $error->getMessage();
            $code = 500;
            DB::rollback();            
        }        

        return createResponse($success, $message, $code, ['id' => $movieUpdate ? $movieUpdate->id() : null]);
    }
}
