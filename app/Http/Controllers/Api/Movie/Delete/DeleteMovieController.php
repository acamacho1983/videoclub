<?php

namespace App\Http\Controllers\Api\Movie\Delete;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Alvaro\Videoclub\Movie\Infrastructure\Model\MovieDAO;
use Alvaro\Videoclub\Movie\Application\Delete\DeleteMovieUseCase;

class DeleteMovieController extends Controller
{
    /**
     * Delete movie.
     *     
     * @return \Illuminate\Http\Response
     */    
    public function __invoke(MovieDAO $movie, DeleteMovieUseCase $useCase)
    { 
        $message = trans('messages.common.delete_data_success');
        $success = true;
        $code = 200;

        DB::beginTransaction();
        try {            
            if (auth()->user()->isAdmin()) {
                $useCase->execute($movie->id); 
                DB::commit();
            } else {
                $success = false;
                $message = "notAuthorized";
                $code = 403;
            }
        } catch (\Throwable $error) {
            $success = false;
            $message = $error;
            $code = 500;
            DB::rollback();            
        }        

        return createResponse($success, $message, $code); 

    }
}
