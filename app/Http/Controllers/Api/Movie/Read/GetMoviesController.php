<?php

namespace App\Http\Controllers\Api\Movie\Read;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alvaro\Videoclub\Movie\Application\Request\GetMoviesRequest;
use Alvaro\Videoclub\Movie\Application\Read\GetMoviesUseCase;

class GetMoviesController extends Controller
{
    /**
     * Get list movies.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(GetMoviesUseCase $useCase, Request $request)
    {         
        $response = $useCase->execute(
            new GetMoviesRequest(
                $request->title,
                $request->genre,
                $request->status,
                $request->createdAtFrom,
                $request->createdAtTo
            )
        );

        return $response->getMovieDetail();
    }
}
