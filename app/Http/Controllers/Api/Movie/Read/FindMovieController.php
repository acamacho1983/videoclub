<?php

namespace App\Http\Controllers\Api\Movie\Read;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alvaro\Videoclub\Movie\Infrastructure\Model\MovieDAO;
use Alvaro\Videoclub\Movie\Application\Read\FindMovieUseCase;

class FindMovieController extends Controller
{
    /**
     * Find movie by id.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(MovieDAO $movie, FindMovieUseCase $useCase)
    {
        $response = $useCase->execute($movie->id); 
        
        if ($response['id'] == 0) {
            return createResponse(false, 'notFound', 404); 
        }         

        return response()->json($response);
    }
}
