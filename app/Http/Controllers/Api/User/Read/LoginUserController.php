<?php

namespace App\Http\Controllers\Api\User\Read;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginUserController extends Controller
{    
    /**
     * Login user.
     *     
     * @return \Illuminate\Http\Response
     */    
    public function __invoke(Request $request) 
    { 
        $credentials = request(['email', 'password']);
        
        if (! $token = auth()->attempt($credentials)) {            
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return respondWithToken($token); 
    }
}
