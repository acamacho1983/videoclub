<?php

namespace App\Http\Controllers\Api\User\Create;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Requests\User\StoreUserRequest;
use Alvaro\Videoclub\User\Application\Request\CreateUserRequest;
use Alvaro\Videoclub\User\Application\Create\CreateUserUseCase;

class RegisterUserController extends Controller
{
    /**
     * Create new user.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(StoreUserRequest $request, CreateUserUseCase $useCase)    
    {
        $message = trans('messages.register.correctly_registered'); 
        $success = true;
        $code = 200;
        
        DB::beginTransaction();
        try {

            $useCase->execute(
                new CreateUserRequest(                
                    $request->name, 
                    $request->first_surname, 
                    $request->second_surname, 
                    'user', 
                    $request->email, 
                    $request->password
                )
            );

            DB::commit();
        } catch (\Throwable $error) {
            $success = false;
            $message = $error->getMessage();
            $code = 500;
            DB::rollback();            
        }        

        return createResponse($success, $message, $code);
    }
}
