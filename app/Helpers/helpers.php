<?php
if (!function_exists('createResponse')) {
    function createResponse($success, $message, $code, $data = [], $json = true) 
    {
        $array = [
            "success" => $success,
            "message" => $message,
            "data" => $data
        ];

        return $json ? response()->json($array, $code) : $array;

    }
}

if (!function_exists('respondWithToken')) {
    function respondWithToken($token) 
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);

    }
}

if (!function_exists('getTokenHeader')) {
    function getTokenHeader($request) 
    {
        return str_replace("XSRF-TOKEN=", "", $request->headers->get('cookie'));
    }
}